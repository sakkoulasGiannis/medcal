<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppoitmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->required();
            $table->string('comment')->nullable();
            $table->integer('company_id')->unsigned();
            $table->integer('entity_id')->unsigned();
            $table->string('color')->default('blue');
            $table->integer('duration')->default(30);
            $table->boolean('approved')->default(0);
            $table->text('phone')->nullable();
            $table->text('email')->nullable();
            $table->date('start_date')->required();
            $table->time('start_time')->required();;
            $table->date('end_date')->nullable();
            $table->time('end_time')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
