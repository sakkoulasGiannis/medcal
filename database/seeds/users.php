<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\User;
use App\Entity;
use Faker\Generator as Faker;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //crete roles
//        Role::create(['name'=>'administrator']);
//        Role::create(['name'=>'manager']);
//        Role::create(['name'=>'user']);

        //permisions
        //can_edit_company


        // create user
        DB::table('users')->insert([
            'name' => "giannis",
            'email' => "info@weborange.gr",
            "password" => Hash::make("manager@!"),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //get User
        $newUser = User::where('name', 'giannis')->first();
        //assign role to user
        $newUser->assignRole('administrator');


        // create user
        DB::table('users')->insert([
            'name' => "admin",
            'email' => "medical@booktool.gr",
            "password" => Hash::make("_e5og7T6"),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //get User
        $newUser = User::where('name', 'users')->first();
        //assign role to user
        $newUser->assignRole('administrator');



        //create a company for the user
//        $company = $newUser->company()->create(['name'=>'17ο', 'title'=>'My company Title']);
        //assign company to user
//        $newUser->update(['company_id'=>$company->id]);
        //create manager
//        $manager = $company->users()->create([
//            'name' => "manager1",
//            'email' => "manager1@weborange.gr",
//            "password" => Hash::make("manager@!"),
//            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
//        ]);
        //add role to manager
//        $manager->assignRole('manager');

//        //create entitis
//        $company->entities()->create([
//                'title'=>'Xoros Eksetasis',
//                'description'=>'Xoros Eksetasis',
//        ]);
//        $company->entities()->create([
//                'title'=>'Xoros Athlisis',
//                'description'=>'Xoros Athlisis',
//        ]);

//        $gymnastirio = Entity::where('id', 1)->first();
//        $iatrio = Entity::where('id', 2)->first();

        //create simple user
//        $simpleUser = $company->users()->create([
//            'name' => "simple",
//            'email' => "simple@weborange.gr",
//            "password" => Hash::make("simple@!"),
//            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
//        ]);

        //create appointment from user side
//        $faker = \Faker\Factory::create();
//        for($a = 0; $a<400; $a++){
//
//
//            $colors = ['green', 'blue', 'red', 'pink', 'purple', 'indigo' , 'teal', 'cyan', 'orange', 'blue-grey' ];
//            $durationMinutes = [30, 60];
//            $duration = array_rand($durationMinutes);
//            $simpleUser->appointment()->create(
//                [
//                    'entity_id'=> $gymnastirio->id,
//                    'title'=> $faker->name(),
//                    'comment'=> $faker->text(200),
//                    'color'=> $colors[array_rand($colors)],
//                    'duration'=> $durationMinutes[$duration] ,
//                    'start_date'=> Carbon::now()->addDay(rand(0, 90))->format('Y-m-d'),
//                    'start_time'=> Carbon::now()->addMinute(rand(0, 480))->format('h:i:s'),
//
//                ]
//            );
//        }
//




    }
}
