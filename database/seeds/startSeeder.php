<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;
use App\Appointment;
class startSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $c1 =  DB::table('companies')->insert([
            'name' => "iatriko-diagnosi-knossou",
            'title' => "ΙΑΤΡΙΚΗ ΔΙΑΓΝΩΣΗ ΚΝΩΣΣΟΣ",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $c2 =  DB::table('companies')->insert([
            'name' => "kentro-irodotos ",
            'title' => "ΔΙΑΓΝΩΣΤΙΚΟ ΚΕΝΤΡΟ ΗΡΟΔΟΤΟΣ",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $c3 =  DB::table('companies')->insert([
            'name' => "agelaki-vasiliki ",
            'title' => "ΑΓΓΕΛΑΚΗ ΒΑΣΙΛΙΚΗ",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'name' => "user1",
            'company_id'=>1,
            'email' => "user1@test.gr",
            "password" => Hash::make("user123"),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $user1 =  DB::table('users')->insert([
            'name' => "user2",
            'company_id'=>2,
            'email' => "user2@test.gr",
            "password" => Hash::make("user123"),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'name' => "user1",
            'company_id'=>3,
            'email' => "user3@test.gr",
            "password" => Hash::make("user123"),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        \App\Company::where('id',1)->first()->entities()->create([
            'title'=>'ΓΕΩΡΓΙΟΣ	ΧΑΤΖΑΚΗΣ',
            'expertise'=>'ΠΝΕΥΜΟΝΟΛΟΓΙΑΣ-ΦΥΜΑΤΙΟΛΟΓΙΑΣ',
            'color'=>'#3F51B5',
            'description'=>'Xoros Eksetasis',
        ]);

        \App\Company::where('id',1)->first()->entities()->create([
            'title'=>'ΕΜΜΑΝΟΥΗΛ	ΚΥΔΩΝΑΚΗΣ',
            'expertise'=>'ΟΡΘΟΠΑΙΔΙΚΗΣ',
            'color'=>'#673AB7',
            'description'=>'Xoros Eksetasis',
        ]);
        \App\Company::where('id',2)->first()->entities()->create([
            'title'=>'ΓΙΑΝΝΙΚΟΣ	ΜΑΛΤΕΖΑΚΗΣ',
            'expertise'=>'ΟΡΘΟΠΑΙΔΙΚΗΣ',
            'color'=>'#2196F3',
            'description'=>'Xoros Eksetasis',
        ]);


        \App\Company::where('id',2)->first()->entities()->create([
            'title'=>'ΑΝΤΩΝΙΟΣ	ΟΡΦΑΝΟΣ',
            'expertise'=>'ΟΡΘΟΠΑΙΔΙΚΗΣ',
            'color'=>'#3F51B5',
            'description'=>'Xoros Eksetasis',
        ]);

        \App\Company::where('id',2)->first()->entities()->create([
            'title'=>'ΕΛΙΣΑΒΕΤ	ΘΑΝΟΥ',
            'expertise'=>'ΠΝΕΥΜΟΝΟΛΟΓΙΑΣ-ΦΥΜΑΤΙΟΛΟΓΙΑΣ',
            'color'=>'#673AB7',
            'description'=>'Xoros Eksetasis',
        ]);
        \App\Company::where('id',2)->first()->entities()->create([
            'title'=>'ΓΕΩΡΓΙΟΣ	ΧΑΤΖΗΔΑΚΗΣ',
            'expertise'=>'ΟΡΘΟΠΑΙΔΙΚΗΣ',
            'color'=>'#2196F3',
            'description'=>'Xoros Eksetasis',
        ]);

        \App\Company::where('id',3)->first()->entities()->create([
            'title'=>'ΑΓΓΕΛΑΚΗ ΒΑΣΙΛΙΚΗ',
            'expertise'=>'ΠΝΕΥΜΟΝΟΛΟΓΙΑΣ-ΦΥΜΑΤΙΟΛΟΓΙΑΣ',
            'color'=>'#009688',
            'description'=>'Xoros Eksetasis',
        ]);



        $colors = ['green', 'blue', 'red', 'pink', 'purple', 'indigo' , 'teal', 'cyan', 'orange', 'blue-grey' ];
        $faker = \Faker\Factory::create();
        for($a = 0; $a<800; $a++){

            $companyid = rand(1,3);
            if($companyid == 1){
                $entity_id = rand(1,2);
            } elseif ($companyid == 2){
                $entity_id = rand(1,2);
            }else{
                $entity_id = 1;
            }
            $durationMinutes = [30, 60];
            $duration = array_rand($durationMinutes);
            Appointment::create(
                [

                    'entity_id'=> $entity_id,
                    'company_id'=> $companyid,
                    'title'=> $faker->name(),
                    'comment'=> $faker->text(100),
                    'approved'=> rand(0,1),
                    'color'=> $colors[array_rand($colors)],
                    'duration'=> $durationMinutes[$duration] ,
                    'start_date'=> Carbon::now()->addDay(rand(0, 90))->format('Y-m-d'),
                    'start_time'=> Carbon::now()->addMinute(rand(0, 480))->format('h:i:s'),

                ]
            );
        }

    }
}
