<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;
class medUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $newUsers = [
            ['name'=>'ΜΑΡΙΑ ΣΥΝΑΔΙΝΑΚΗ'	,'mail'=>'msynadinaki@idk.gr'		,'pass'=>'ms2020'],
            ['name'=>'ΑΝΤΩΝΑΚΑΚΗ ΚΑΛΛΙΟΠΗ'	,'mail'=>'kantonakaki@idk.gr'		,'pass'=>'ka2020'],
            ['name'=>'ΚΛΙΝΑΚΗ ΑΝΤΙΓΟΝΗ'	,'mail'=>'aklinaki@idk.gr'			,'pass'=>'ak2020'],
            ['name'=>'ΓΙΩΡΓΑΛΗ ΣΤΑΜΑΤΙΝΑ'	,'mail'=>'sgiorgali@idk.gr'		,'pass'=>'sg2020'],
            ['name'=>'ΚΟΚΟΛΑΚΗ ΜΑΡΙΑ'	,'mail'=>'mkokolaki@idk.gr'			,'pass'=>'mk2020'],
            ['name'=>'ΣΜΥΡΝΑΚΗ ΙΩΑΝΝΑ'	,'mail'=>'ismirnaki@idk.gr'		,'pass'=>'is2020'],
            ['name'=>'ΔΑΝΕΛΑΚΗ ΑΝΝΑ'	,'mail'=>'adanelaki@idk.gr'		,'pass'=>'ad2020'],
            ['name'=>'ΟΡΦΑΝΟΥ ΜΑΡΙΑ'	,'mail'=>'morfanou@idk.gr'		,'pass'=>'mo2020'],
            ['name'=>'ΠΑΤΕΡΟΠΟΥΛΟΥ ΔΕΣΠΟΙΝΑ'	,'mail'=>'dpateropoulou@idk.gr'		,'pass'=>'dp2020'],
            ['name'=>'ΦΡΑΓΚΑΚΗ ΧΡΙΣΤΙΝΑ'	,'mail'=>'cfragaki@idk.gr'		,'pass'=>'cf2020'],
            ['name'=>'ΜΟΥΡΤΖΑΚΗ ΑΝΝΑ'	,'mail'=>'amourtzaki@idk.gr'		,'pass'=>'am2020'],
            ['name'=>'ΜΑΡΚΑΚΗ ΕΥΑΓΓΕΛΙΑ'	,'mail'=>'emarkaki@idk.gr'		,'pass'=>'em2020'],
            ['name'=>'ΜΑΝΩΛΑΡΑΚΗ ΕΥΑΓΓΕΛΙΑ'	,'mail'=>'emanolaraki@idk.gr'		,'pass'=>'em2020'],
            ['name'=>'ΔΑΜΙΑΝΑΚΗ ΝΕΚΤΑΡΙΑ'	,'mail'=>'ndamianaki@idk.gr'		,'pass'=>'nd2020'],
            ['name'=>'ΚΑΜΠΑΝΑΡΑΚΗ ΜΑΡΙΑ'	,'mail'=>'mkampanaraki@idk.gr'		,'pass'=>'mk2020'],
            ['name'=>'ΚΑΝΤΙΔΑΚΗ ΑΝΔΡΟΝΙΚΗ'	,'mail'=>'akantidaki@idk.gr'		,'pass'=>'ak2020'],
        ];

        foreach ($newUsers as $u){
            DB::table('users')->insert([
                'name' => $u['name'],
                'email' => $u['mail'],
                "password" => Hash::make($u['pass']),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            $newUser = User::where('email', $u['mail'])->first();
            //assign role to user
            $newUser->assignRole('manager');
        }


    }
}


