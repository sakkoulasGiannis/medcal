<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Contracts\UserResolver;



class User extends Authenticatable implements MustVerifyEmail , Auditable, UserResolver
{
    use Notifiable;
    use HasRoles;
    use \OwenIt\Auditing\Auditable;

    public static function resolve(){

    }
    /**
     * {@inheritdoc}
     */
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function companies(){
        return $this->belongsToMany(Company::class, 'company_users');
    }

    public function entities(){
        return $this->belongsToMany(Entity::class, 'entity_users');
    }

    public function company(){
        return $this->hasOne(Company::class, 'id', 'company_id');
        return $this->hasOne(Company::class, 'user_id', 'id');
    }

    public function appointment(){
        return $this->hasMany(Appointment::class, 'user_id', 'id');
    }


}
