<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliend extends Model
{
    protected $fillable = ['name','amka','email', 'phone', 'birthdate'];
    public function appointments(){
        return $this->hasMany(Appointment::class, 'cliend_id', 'id');
    }
}
