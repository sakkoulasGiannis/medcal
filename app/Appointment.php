<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;
use App\Cliend;

class Appointment extends Model implements AuditableContract
{
    use SoftDeletes;
    use Auditable;

    protected $fillable = ['entity_id', 'cliend_id','email', 'phone','user_id', 'company_id', 'title', 'comment', 'start_date', 'end_date', 'start_time','end_time', 'duration', 'color', 'approved', 'amka'];

    protected $appends = ['startTimeTimestamp', 'endTimeTimestamp'];

    public function getStartTimeTimestampAttribute()
    {
        return strtotime( $this->start_time);
    }
    public function getEndTimeTimestampAttribute()
    {
        return strtotime( $this->end_time);
    }


    public function getStartTimeAttribute($value){
        return substr($value, 0, 5);
    }
    public function user(){
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function cliend(){
        return $this->hasOne(Cliend::class, 'id', 'cliend_id');
    }

    public function entities(){
        return $this->hasOne(Entity::class, 'id', 'entity_id');
    }
    public function entity(){
        return $this->belongsTo(Entity::class, 'id', 'entity_id');
    }

}
