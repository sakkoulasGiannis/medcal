<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ManagerController extends Controller
{
    protected function create(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'manager' => [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]
        ]);

        $manager =  $request->get('manager');


        if ($validator->passes()) {
//            $company = Auth::user()->company;
            $user = User::create([
                'name' => $manager['name'],
                'email' => $manager['email'],
                'password' => Hash::make($manager['password']),
            ]);
            $user->assignRole($manager['role']);
            $user->givePermissionTo($request->get('permissions'));
            return $user;
        } else {
            return "Something went wrong";
        }

        //get User
        //assign role to user

//        $user->companies()->sunc([$company->id]);

        return response()->json(['error' => $validator->errors()->all()]);

    }
}
