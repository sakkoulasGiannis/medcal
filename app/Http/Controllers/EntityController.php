<?php

namespace App\Http\Controllers;

use App\Company;
use App\Entity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EntityController extends Controller
{

    public function index(){
        if(Auth::user()->hasRole('administrator')){
            $entities = Entity::get();
        }else{
            $company = Auth::user()->companies->pluck('id');
            $entities =  Entity::whereIn('company_id', $company)->get();
        }

        return view('entity', compact('entities'));
    }

    public function update( Entity $entity, Request $request){
        $entity->update($request->all());
        return $entity;
    }

    public function create(Request $request){
        if($request->has('company_id')){
            $company = Company::find($request->get('company_id'));
        }else{
            $company = Auth::user()->company;
        }
        $company = $company->entities()->create($request->get('entity'));
        $company->update(['icalKey'=> implode('', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6))]);
        return $company;
    }

    public function json_format_for_company(Request $request){
        $auth = Auth::user();

        if($request->has('company_id') && $request->get('company_id') != null){

            return Entity::with('company')->where('company_id', $request->get('company_id'))
                ->whereIn('id', $auth->entities()->pluck('id'))
                ->orderBy('title')
                ->get();

        }elseif($auth->hasRole('manager')){

            return Entity::with('company')->whereIn('id', $auth->entities()->pluck('id'))
                ->orderBy('title')
                ->get();


        }elseif($auth->hasRole('administrator')){

            return Entity::with('company')->get();
        }else{

            $auth = Auth::user();
            return Entity::with('company')->where('company_id', $auth->company->id)->orderBy('title')->get();

        }

    }
    public function json_format(){

         $auth = Auth::user();
         if(!$auth){
             return null;
         }
         if($auth->hasRole('manager')){

             return Entity::whereIn('company_id', $auth->companies()->pluck('id'))
                 ->whereIn('id', $auth->entities()->pluck('id'))
                 ->orderBy('title')
                 ->get();
         }else{
//            return Entity::where('company_id', $auth->company->id)->get();
         }
    }

    public function expertise_json(){
        return Entity::select('expertise')
            ->groupBy('expertise')->get();
    }

    public function entities_json(){
        return Entity::select('id', 'title', 'expertise')->get();
    }

    public function expertise_json_by_company(Request $request){

        return Entity::with('timetable')->select('id', 'title', 'expertise', 'duration')->where('company_id', $request->get('company_id'))->get();
    }
}
