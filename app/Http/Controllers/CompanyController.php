<?php

namespace App\Http\Controllers;

use App\Company;
use App\Entity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{


    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */

    public function create(Request $request)
    {
        $auth = Auth::user();
        $company = Company::create($request->all());
        $auth->company_id = $company->id;
        $auth->save();
        $auth->assignRole('administrator');

        return redirect('/calendar');
    }

    public function managers()
    {
        return view('managers');
    }

    public function updateManager(Request $request, User $user)
    {
        return $user;
        return $request->all();

    }




    public function companies()
    {
        return view('companies');
    }

    public function company(Company $company)
    {

        return view('company', compact('company'));
    }



    public function companies_entities_json(Request $request)
    {
        return Entity::where('company_id', $request->get('company_id'))->get();
    }

    public function company_json()
    {
        return Company::with('entities')->get();

    }

    public function getManagers()
    {

        if (Auth::user()->hasRole('administrator')) {
            return User::with('entities', 'company')->get();
        }

        if (Auth::user()->hasRole('manager') && Auth::user()->hasPermissionTo('ViewEntities')) {

            return User::with('entities', 'company')->whereHas('roles', function ($query) {
                return $query->where('name', '!=' , 'administrator');
            })->get();
        }
        if(Auth::user()->company){
            $company = Auth::user()->company;
            return User::where('company_id', $company->id)->get();
        }

    }


}
