<?php

namespace App\Http\Controllers;

use App\Cliend;
use Illuminate\Http\Request;
use App\User;
class CliendController extends Controller
{
    public function index(){

        return view('clients');
    }


    public function search(Request $request){

        $q = $request->get('q');
        if(mb_strlen($q) > 3){
            $client = Cliend::where('amka','like', '%'.$q.'%')
                ->orWhere('name','like', '%'.$q.'%')
                ->orWhere('phone','like', '%'.$q.'%')->get()
                ->take(40);
            return $client;
        }

    }

    public function calendar(Cliend $cliend, Request $request){
        return $cliend->appointments()->with('entities')->orderBy('start_date', 'DESC')->get();
    }

    public function update(Cliend $client, Request $request){

        return $client->update($request->input());
    }
}
