<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Cliend;
use App\Company;
use App\Entity;
use App\Mail\newAppointment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\AppointmentResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\AppointmentBResource;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Facades\Mail;

class AppointmentController extends Controller
{

    public function ical($key, Request $request)
    {
        if (!isset($key)) {
            return "nothing found";
        }
        $entity = Entity::where('icalKey', $key)->first();
        $calendarName = $entity->title;
        $vCalendar = new Calendar('www.booktool.gr');
        $vCalendar->setName($calendarName);
        $vCalendar->setTimezone('Europe/Athens');


        $appointments = Appointment::where('entity_id', $entity->id)->whereDate('created_at', '>=', \Carbon\Carbon::today()->subWeek())->get();
        foreach ($appointments as $a) {
            $vEvent = new Event($a->id);
            $start_at = Carbon::parse($a->start_date . ' ' . $a->start_time);
//            $start_at = Carbon::parse($a->start_date . ' ' . $a->start_time);
            $end_at = Carbon::parse($a->end_date . ' ' . $a->end_time);
//            $end_at = Carbon::parse($a->end_date . ' ' . $a->end_time);
            $vEvent
                ->setDtStart($start_at)
                ->setDtEnd($end_at)
                ->setNoTime(false)
                ->setSummary($a->title);

            $vCalendar->addComponent($vEvent);
        }


        header('Content-Type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename="cal.ics"');
        return $vCalendar->render();
    }

    public function calendar()
    {
        $auth = Auth::user();


        if (!$auth) {
            return redirect('/login');
        } elseif (!$auth->company && $auth->hasRole('administrator')) {
            return redirect('/companies');
        } elseif ($auth && $auth->company) {
            $company = $auth->company;
            return redirect('show/' . $company->name);
        } elseif ($auth->hasRole('manager')) {
            return view('calendar');
        } elseif (!$auth->company) {
            return 'Please Contact with the Administrator';
        } elseif ($auth) {
            return view('calendar');
        }

    }


    public function spesificCalendar(Company $company)
    {


        $auth = Auth::user();
        if (!$auth) {
            return redirect('/login');
        } elseif ($auth) {
            return view('spesific-calendar', compact('company'));
        }
    }


    public function appointments_json(Request $request)
    {

        $auth = Auth::user();
        if (!$auth) {
            return redirect('/login');
        }
        if ($request->has('company_id') && $request->get('company_id') != null) {
            $company = [$request->get('company_id')];
        } elseif ($auth->hasRole('manager')) {
            $company = $auth->companies()->pluck('id');
        } else {
            $company = $auth->companies()->pluck('id');
        }




        if ($request->has('entity') && $request->entity != null) {

//            $cacheName = 'entity_'.$request->entity.'_'.$request->start['date'].'_'.$request->end['date'];
//            if (Cache::has($cacheName)) {
//                return Cache::get($cacheName);
//            }else{
                $appointmens = AppointmentResource::collection(Appointment::whereIn('company_id', $company)->where('entity_id', $request->entity)->whereBetween('start_date', [$request->start['date'], $request->end['date']])->get());
//                Cache::set($cacheName, $appointmens);

                return $appointmens;
//            }


        }

//        $cacheName = 'all_'.$request->start['date'].'_'.$request->end['date'];
//        if(Cache::has($cacheName)){
//
//            return Cache::get($cacheName);
//        }else{

             $allAppointments =  AppointmentBResource::collection(Appointment::with('entities')
                ->whereIn('company_id', $company)
                ->whereIn('entity_id', $auth->entities()->pluck('id'))
                ->whereBetween('start_date', [$request->start['date'], $request->end['date']])
                ->get());
//            Cache::set($cacheName, $allAppointments);


            return $allAppointments;
//        }


    }

    public function AppointmentAudits(Appointment $appointment)
    {
        return $appointment->audits()->with('user')->orderBy('created_at', 'desc')->get();
    }

    public function get(Appointment $appointment)
    {



        //@todo check if user can see endities
        if (!Auth::user()) {
            return null;
        }
        $appointment = Appointment::find($appointment->id);
        $clind = Cliend::find($appointment->cliend_id);
        if($clind){
            $appointment->amka = $clind->amka;
            $appointment->title = $clind->name;
            $appointment->phone = $clind->phone;
            $appointment->email = $clind->email;
            $appointment->birthday = $clind->birthday;
        }
        return $appointment;
    }

    public function create(Request $request)
    {

         if($request->get('duration') == 'Επιλογή'){
            $request->request->add(['duration' => 0]); //add request
        }
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255'],
            'duration' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required']
        ]);

        if( $request->has('amka') && $request->get('amka') != '' && $request->get('amka') != null){
            $client = Cliend::where('amka', $request->get('amka'))->first();
        }else{
            $client = Cliend::where('name', $request->get('title'))->where('phone', $request->get('phone'))->first();
        }

        $amka = ($request->get('amka') == '' || $request->get('amka') == null )? '***'.time():$request->get('amka');
        if (!$client) {
            $client = Cliend::create([
                'name' => $request->get('title'),
                'amka' => $amka,
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'birthdate' => $request->get('birthday'),
            ]);
        }
        if ($validator->passes()) {

//            $company = Auth::user()->company;
            $entity = Entity::find($request->get('entity_id'));
            $company = $entity->company;
            $appointment = array_merge($request->all(), ['amka'=>$amka,'company_id' => $company->id, 'cliend_id' => $client->id]);
            $appointment = Appointment::create($appointment);
            //send email
            try {
                if($request->has('email') && $request->get('email') != ''){

                    $data = [
                        'entity' => $entity->title,
                        'company'  => $company->title,
                        'date'  => $appointment->start_date. " ".$appointment->start_time,
                        'phone'   => $company->phone,
                        'phone_2'   => $company->phone_2,
                        'address'   => $company->address,
                        'email' => $company->email,
                        'client' => $request->get('title')
                    ];

//                    Mail::to($request->get('email'))->send(new newAppointment($data));
                }
            }catch(Throwable $e){
                report($e);
            }

            Cache::flush();


            return $appointment;
        }


        return response()->json(['error' => $validator->errors()->all()]);

    }

    public function update(Appointment $appointment, Request $request)
    {

        if($request->has('duration') && $request->get('duration') == 'Επιλογή'){
            $request->request->add(['duration' => 0]); //add request
        }

        if (substr( $appointment->amka, 0, 3 ) === "***") {

            $client = Cliend::where('amka',$appointment->amka)->first();
            if($client){
                $client->update([
                    'name' => $request->get('title'),
                    'amka' => $request->get('amka'),
                    'email' => $request->get('email'),
                    'phone' => $request->get('phone'),
                    'birthdate' => $request->get('birthday'),
                ]);
            }else{
                if( $appointment->cliend){
                    $request->request->add(['amka' => $appointment->cliend->amka]);
                }
            }

        }elseif(substr( $appointment->amka, 0, 3 ) !== "***" && $appointment->amka != null){
            $client = Cliend::where('amka', $appointment->amka)->first();
        }else{
             $client = Cliend::where('name', $request->get('title'))->where('phone', $request->get('phone'))->first();
        }



        if (!$client) {
            $amka = ($request->get('amka') == '' || $request->get('amka') == null )? '***'.time():$request->get('amka');
            $newCliend = Cliend::create([
                'name' => $request->get('title'),
                'amka' => $amka,
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'birthdate' => $request->get('birthday'),
            ]);

            $request->request->add(['cliend_id' => $newCliend->id]);
            $request->request->add(['amka' => $amka]);


        }elseif($appointment->cliend_id != $client->id){
            $request->request->add(['cliend_id' => $client->id]);
        }


        $appointment->update($request->all());
//        Cache::flush();
        return $appointment;
    }

    /*
     * save appointment from booking form
     */
    public function saveAppointment(Request $request)
    {
        Appointment::create($request->all());
        return redirect('/booking/success');
    }

    public function successAppointment()
    {
        return view('success');
    }

    public function pendings()
    {
        if (Auth::user()->hasRole('administrator')) {

            return Appointment::where('approved', 0)
                ->orderBy('start_date', 'asc')
                ->get();
        } else {
            $company = Auth::user()->companies->pluck('id');
            return Appointment::whereId('company_id', $company)
                ->orderBy('start_date', 'asc')
                ->where('approved', 0)
                ->get();
        }

    }

    public function booking()
    {
        return view('booking');
    }

    public function bookAppointment(Request $request)
    {
        $user = Auth::user();
        if ($user) {
            $appointment = array_merge($request->all(), ['user_id' => $user->id]);
        }
        $company = Company::where('id', $request->get('company_id'))->first();

        return Appointment::create($appointment);
    }

    public function joinMeetup(Request $request)
    {
        $roomname = $request->r;
        return view('meetup', compact('roomname'));
    }

    public function getAppointmentsForDay(Request $request)
    {

        $entity = Entity::find($request->get('entity_id'));
        $closeTimes = Appointment::where('entity_id', $request->get('entity_id'))
            ->where('start_date', $request->get('start_date'))
            ->when($request->has('appointment_id') && $request->get('appointment_id') != '', function ($query) use ($request) {
                $query->where('id', '!=', $request->get('appointment_id'));
            })
            ->get();

        $start = strtotime('06:00');
        $end = strtotime('23:05');

        $time = [];
        $duration = ($request->has('duration')) ? $request->get('duration') : $entity->duration;

        for ($i = $start; $i < $end; $i = $i + 5 * 60) {
            $stime = date('H:i', $i);
            $plusDurationMinutes = strtotime("+ {$duration} minutes", $i);
            $a = $closeTimes
                ->where('startTimeTimestamp', '<', $plusDurationMinutes)
                ->where('endTimeTimestamp', '>', $i)
                ->first();
            if (!$a) {
                $time[] = '' . $stime;
            }
        }

        return $time;
    }

    function calculateTime(Request $request)
    {
        $entity = Entity::find($request->get('entity_id'));
        $closeTimes = Appointment::where('entity_id', $request->get('entity_id'))
            ->where('start_date', $request->get('start_date'))
            ->get();

        $start = strtotime('06:00');
        $end = strtotime('23:05');

        $time = [];
        $duration = ($request->has('duration')) ? $request->get('duration') : $entity->duration;

        for ($i = $start; $i < $end; $i = $i + 5 * 60) {
            $stime = date('H:i', $i);
            $plusDurationMinutes = strtotime("+ {$duration} minutes", $i);
            $a = $closeTimes
                ->where('startTimeTimestamp', '<', $plusDurationMinutes)
                ->where('endTimeTimestamp', '>', $i)
                ->first();
            if (!$a) {
                $time[] = '' . $stime;
            }
        }

        return $time;

    }

    public function delete(Appointment $appointment)
    {

         $appointment->delete();
        Cache::flush();
        return "success delete";
    }


}
