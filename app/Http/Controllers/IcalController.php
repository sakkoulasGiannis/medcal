<?php

namespace App\Http\Controllers;

use App\Entity;
use App\Mail\IcalKey;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Mail;
class IcalController extends Controller
{
    public function sendMail(Request $request, Entity $entity){

        $user = User::find(1);
        Mail::to($user)->send(new IcalKey($entity));


    }
}

