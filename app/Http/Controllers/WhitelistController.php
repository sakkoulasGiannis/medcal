<?php

namespace App\Http\Controllers;

use App\Whitelist;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class WhitelistController extends Controller
{

    public function searchEmail(Request $request){
        $email = Whitelist::where('email', $request->get('email'))->first();
        if($email){
            return ['status'=>true, 'company'=> $email->company_id];
        }
        return ['status'=>false];
    }

    public function searchUser(Request $request){

        $user = User::where('email', $request->get('email'))->first();

        if($user && Auth::check()){
            return ['status'=>true, 'login'=>'no'];
        }elseif($user){
            return ['status'=>true, 'login'=>'yes'];
        }
        return ['status'=>false, 'login'=>'no'];
    }
}
