<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    public function check($permissionName){

        if (! Auth::user()->hasPermissionTo($permissionName)) {
            return false;
        }
        return true;
    }
}
