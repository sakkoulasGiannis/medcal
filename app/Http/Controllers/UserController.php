<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function userIsOnline()
    {
        return Auth::user();
    }

    public function confirmEmail()
    {
        return view('/confirmemail');
    }

    public function updateUser(Request $request, User $user)
    {


        if ($request->has('companies') && count($request->get('companies')) > 0) {
            $user->companies()->sync($request->get('companies'));
        }

        if ($request->has('entities') && count($request->get('entities')) > 0) {
            $user->entities()->sync($request->get('entities'));
        }
    }

    public function entities(User $user)
    {
        return $user->entities;
    }

    public function entitiesId(User $user)
    {
        return $user->entities()->pluck('id');
    }

    public function companies(User $user)
    {
        return $user->companies;
    }

    public function companiesId(User $user)
    {
        return $user->companies()->pluck('id');
    }

    public function update(Request $request, User $user)
    {
        $data = $request->get('manager');
        if (isset($data['password'])) {
            $user->update([
                'password' => hash::make($data['password'])
            ]);
        }

    }

    public function destroy($id){
        $deleted = User::find($id)->delete();
        return true;

    }
}
