<?php

namespace App\Http\Resources;

use App\Cliend;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentBResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $color = (isset($this->entities) && $this->entities->color) ? $this->entities->color : $this->color;
        return [
            'id' => $this->id,
            'name' => $this->title . " ".$this->phone . ' '. $this->comment,
            'comment' => $this->comment,
            'duration' => $this->duration,
            'start' => $this->start_date . " " . $this->start_time,
            'end' => $this->end_date . " " . $this->end_time,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
//            'end'=> ($this->end_date != null && $this->end_time != null)?$this->end_date . " ".$this->end_time: $this->start_date. " ". Carbon::parse($this->start_time)->addMinutes($this->duration)->format('H:i:s'),
            'color' => ($this->approved) ? $color : 'grey darken-1',
            'cliend'=>$this->cliend_id,
//            'cliend'=>Cliend::find($this->cliend_id),
        ];
    }
}
