<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name', 'title', 'description', 'logo', 'email', 'phone'];

    public function getRouteKeyName()
    {
        return 'name';
    }

//    public function users (){
//        return $this->hasMany(User::class , 'company_id', 'id');
//    }

    public function users(){
        return $this->belongsToMany(User::class, 'company_users');
    }

    public function owner(){
        return $this->belongsTo(User::class, 'id', "user_id");
    }

    public function entities(){
        return $this->hasMany(Entity::class, 'company_id', 'id');
    }


    public function whitelists(){
        return $this->hasMany(Whitelist::class, 'company_id', 'id');
    }

    public function managers(){
        return $this->hasMany(Manager::class, 'company_id', 'id');
    }

    public function appointments(){
        return $this->hasMany(Appointment::class, 'manager_id', 'id');
    }
}
