<?php
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('login', )
Route::get('/permission/{permissionName}', 'PermissionController@check');

Route::get('/ical/send/{entity}', 'IcalController@sendMail');
Route::get('/ical/{key}', 'AppointmentController@ical');
Route::get('/confirmemail', 'UserController@confirmEmail');
Route::get('/userisonline', 'UserController@userIsOnline');
Route::post('/client/search', 'CliendController@search');
Route::get('/clients', 'CliendController@index');
Route::put('/clients/{client}', 'CliendController@update');
Route::post('/client/search/{cliend}/appointments', 'CliendController@calendar');

Route::post('/booking/add', 'AppointmentController@saveAppointment');
Route::get('/booking/success', 'AppointmentController@successAppointment');

Route::get('/show/{company}', 'AppointmentController@spesificCalendar')->name('spesificCalendar');
Route::get('/company', 'CompanyController@company');
Route::post('/company', 'CompanyController@create')->name('createCompany');
Route::get('/company/managers', 'CompanyController@managers');
Route::get('/company/users', 'CompanyController@getManagers');
Route::get('/company/users/{user}/entities', 'UserController@entities');
Route::get('/company/users/{user}/entitiesId', 'UserController@entitiesId');
Route::get('/company/users/{user}/companiesId', 'UserController@companiesId');
Route::get('/company/users/{user}/companies', 'UserController@companies');
Route::post('/company/users/{user}', 'UserController@updateUser');
Route::get('/company/{company}', 'CompanyController@company');

Route::post('/company/managers/create', 'ManagerController@create');
Route::post('/company/managers/{user}/update', 'UserController@update');
Route::post('/company/managers/{id}/destroy', 'UserController@destroy');
//Route::post('/company/{company}', 'CompanyController@get_compan');
Route::get('/companies/', 'CompanyController@companies');
Route::get('/companies_json', 'CompanyController@company_json');
Route::post('/companies_entities_json', 'CompanyController@companies_entities_json');

Route::get('/entity', 'EntityController@index');
Route::post('/entity/create', 'EntityController@create');
Route::post('/entity/{entity}/update', 'EntityController@update');
Route::post('/entity/{company}', 'EntityController@update');
Route::get('/calendar', 'AppointmentController@calendar')->name('calendar');
Route::get('/calendar/pendings', 'AppointmentController@pendings');
Route::post('/calendar/appointments_json', 'AppointmentController@appointments_json');
Route::post('/calendar/{appointment}/audits', 'AppointmentController@AppointmentAudits');
Route::get('/calendar/get/{appointment}', 'AppointmentController@get');
Route::get('/calendar/{appointment}', 'AppointmentController@get');

Route::post('/appointment/', 'AppointmentController@create');
Route::post('/appointment/{appointment}/delete', 'AppointmentController@delete');
Route::post('/appointment/{appointment}', 'AppointmentController@update');

Route::get('/entities/json_format', 'EntityController@json_format');
Route::post('/entities/json_format', 'EntityController@json_format_for_company'); // use this to filter company entities
Route::get('/expertise_json', 'EntityController@expertise_json');
Route::post('/expertise_json_by_company', 'EntityController@expertise_json_by_company');
Route::get('/entities_json/{expertise}', 'EntityController@entities_json_by_expertise');

Route::post('/bookAppointment', 'AppointmentController@bookAppointment');
Route::get('meetup','AppointmentController@joinMeetup');
Route::post('/get-appointments-for-day', 'AppointmentController@getAppointmentsForDay');
Route::get('/calculatetime', 'AppointmentController@calculateTime');

Route::post('/searchwhitelist', 'WhitelistController@searchEmail');
Route::post('/searchUser', 'WhitelistController@searchUser');

//
//Route::get('/vue', function(){
//    return view('index');
//});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
