/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vuetify from 'vuetify'
import VueToastr from '@deveodk/vue-toastr'
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'
import VueTimepicker from 'vue2-timepicker/dist/VueTimepicker.common.js'
import 'vue2-timepicker/dist/VueTimepicker.css'

 require('./bootstrap');

window.Vue = require('vue');

Vue.use(VueTimepicker);
Vue.use(Vuetify);
window.moment = require('moment');
window.moment.locale('el');


Vue.use(VueToastr, {
    defaultPosition: 'toast-top-right',
    defaultType: 'info',
    defaultTimeout: 5000
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css'
import VueClipboard from 'vue-clipboard2'

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('index', require('./components/index.vue').default);
Vue.component('calendar', require('./components/calendar.vue').default);
Vue.component('navigation', require('./components/navigation').default);
Vue.component('managers', require('./components/managers').default);
Vue.component('company', require('./components/company').default);
Vue.component('companies', require('./components/companies').default);
Vue.component('loginforms', require('./components/auth').default);
Vue.component('registerforms', require('./components/register').default);
Vue.component('bookingform', require('./components/bookingformold').default);
Vue.component('entities', require('./components/entities').default);
Vue.component('meetup', require('./components/meeting').default);
Vue.component('clients', require('./components/clients').default);

Vue.use(VueClipboard)


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const ignoreWarnMessage = 'The .native modifier for v-on is only valid on components but it was used on <div>.';
Vue.config.warnHandler = function (msg, vm, trace) {
    // `trace` is the component hierarchy trace
    if (msg === ignoreWarnMessage) {
        msg = null;
        vm = null;
        trace = null;
    }
}


const app = new Vue({
    el: '#app',
    vuetify: new Vuetify({})
})

