<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
        'userId' => Auth::user()->id,
        'permissions' => Auth::user()->permissions()->pluck('name')->toArray()
    ]); ?>
</script>

<script src="{{ asset('js/manifest.js')}}"></script>
<script src="{{ asset('js/vendor.js')}}"></script>
<script src="{{ asset('js/app.js?v=').time()}}"></script>