@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
              <h2>Σας Ευχαριστούμε</h2>
                <p >
                    Το αίτημα σας για ραντεβού έχει καταχωρηθεί επιτυχώς. <br/>
                    Θα ενημερωθείτε μετά τον έλεγχο.
                </p>
            </div>
        </div>
    </div>

    <style>
        .datepicker table, .datepicker-inline{
            width: 100%;
        }
        td {
            border: solid 1px #ddd!important;
            border-radius: 0!important;
        }

        .datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
            background: #E91E63;
            color: #fff;
            border-color: #cc1a56!important;
            border-radius: 0;
        }
    </style>
@endsection

@section('script')
    <script>

        var date = new Date();
        date.setDate(date.getDate());

        jQuery(document).ready(function(){
            $('.phone').mask('0000000000', {
                placeholder: "##########"
            });
            $('.input-group.date').datepicker({
                format: 'yyyy-mm-dd',
                startDate: date,
                language: "el",
                keyboardNavigation: false,
                daysOfWeekDisabled: "0",
                todayHighlight: true,
                datesDisabled: ['03/06/2020', '27/03/2020']
            })
        })
        $('#sandbox-container div').on("changeDate", function(event) {
            console.log(event.date);
        });
        $('#sandbox-container div').on("changeMonth", function(event) {
            console.log(event);
        });

    </script>
@endsection
